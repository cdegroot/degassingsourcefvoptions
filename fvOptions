/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  6                                     |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      fvOptions;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

degassingMassSource
{
    type            scalarCodedSource;
    active          yes;
    selectionMode   all;

    name            degassingMassSource;
    patches         (outlet);
    rhoName         thermo:rho.air;
    alphaName       alpha.air;

    scalarCodedSourceCoeffs
    {
        selectionMode   all;

        fields          (thermo:rho.air);

        codeInclude
        #{
             #include "fvm.H"
        #};

        codeCorrect
        #{

        #};

        codeAddSup
        #{
             // Get the names of the patches on which to apply the degassing forces
             DynamicList<word, 1, 0> patches;
             coeffs().lookup("patches") >> patches;

             // Get the required fields
             const word rhoName = coeffs().lookup("rhoName");
             const volScalarField& rhoAir = mesh().lookupObject<volScalarField>(rhoName);
             const word alphaName = coeffs().lookup("alphaName");
             const volScalarField& alphaAir = mesh().lookupObject<volScalarField>(alphaName);

             // Get the timestep
             const scalar deltaT = mesh().time().deltaT().value();

             // Create degassing mass source coefficient and initialize to zero
             volScalarField degassingMassSourceCoeff
             (
                 IOobject
                 (
                     "degassingMassSourceCoeff",
                     mesh().time().timeName(),
                     mesh(),
                     IOobject::NO_READ,
                     IOobject::AUTO_WRITE
                 ),
                 mesh(),
                 dimensionedScalar("degassingMassSourceCoeff", dimless/dimTime, 0.0)
             );

             // Compute the degassing mass source coefficient for each cell adjacent to the selected patches
             forAll(patches, iPatch)
             {
                 // Get the boundary patch
                 const fvPatch& patch = mesh().boundary()[patches[iPatch]];

                 // Loop through each boundary face and compute degassing force coefficient in adjacent cell
                 forAll(patch, iFace)
                 {
                     label iCell = patch.faceCells()[iFace];
                     degassingMassSourceCoeff[iCell] = -alphaAir[iCell]/deltaT;
                 }
             }

             // Add the degassing force term
             eqn += fvm::Sp(degassingMassSourceCoeff, rhoAir);
        #};

        codeSetValue
        #{
        
        #};

        // Dummy entry. Make dependent on above to trigger recompilation
        code
        #{
            $codeInclude
            $codeCorrect
            $codeAddSup
            $codeSetValue
        #};
    }
}

degassingForce
{
    type            vectorCodedSource;
    active          yes;
    selectionMode   all;

    name            degassingForce;
    patches         (outlet);
    rhoName         thermo:rho.air;
    alphaName       alpha.air;
    UName           U.air;

    vectorCodedSourceCoeffs
    {
        selectionMode   all;

        fields          (U.air);

        codeInclude
        #{
             #include "fvm.H"
        #};

        codeCorrect
        #{

        #};

        codeAddSup
        #{
             // Get the names of the patches on which to apply the degassing forces
             DynamicList<word, 1, 0> patches;
             coeffs().lookup("patches") >> patches;

             // Get the required fields
             const word rhoName = coeffs().lookup("rhoName");
             const volScalarField& rhoAir = mesh().lookupObject<volScalarField>(rhoName);
             const word alphaName = coeffs().lookup("alphaName");
             const volScalarField& alphaAir = mesh().lookupObject<volScalarField>(alphaName);
             const word UName = coeffs().lookup("UName");
             const volVectorField& UAir = mesh().lookupObject<volVectorField>(UName);

             // Get the timestep
             const scalar deltaT = mesh().time().deltaT().value();

             // Create degassing force coefficient and initialize to zero
             volScalarField degassingForceCoeff
             (
                 IOobject
                 (
                     "degassingForceCoeff",
                     mesh().time().timeName(),
                     mesh(),
                     IOobject::NO_READ,
                     IOobject::AUTO_WRITE
                 ),
                 mesh(),
                 dimensionedScalar("degassingForceCoeff", dimDensity/dimTime, 0.0)
             );

             // Compute the degassing force coefficient for each cell adjacent to the selected patches
             forAll(patches, iPatch)
             {
                 // Get the boundary patch
                 const fvPatch& patch = mesh().boundary()[patches[iPatch]];

                 // Loop through each boundary face and compute degassing force coefficient in adjacent cell
                 forAll(patch, iFace)
                 {
                     label iCell = patch.faceCells()[iFace];
                     degassingForceCoeff[iCell] = -rhoAir[iCell]*alphaAir[iCell]/deltaT;
                 }
             }

             // Add the degassing force term
             eqn += fvm::Sp(degassingForceCoeff, UAir);
        #};

        codeSetValue
        #{
        
        #};

        // Dummy entry. Make dependent on above to trigger recompilation
        code
        #{
            $codeInclude
            $codeCorrect
            $codeAddSup
            $codeSetValue
        #};
    }
}


// ************************************************************************* //

